<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuthToken extends Model
{
    protected $fillable = [
        'user',
        'token',
        'expire',
        'created_at',
        'updated_at'
    ];
}
