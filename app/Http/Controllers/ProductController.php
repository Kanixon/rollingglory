<?php

namespace App\Http\Controllers;

use App\Http\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\This;

class ProductController extends Controller
{
    protected ProductService $productService;

    public function __construct(
        ProductService $productService
    ) {
        $this->productService = $productService;
    }

    public function getAll(Request $request)
    {
        $products = $this->productService->getAll($request);
        return $this->successWithData($products);
    }

    public function get($id)
    {
        $product = $this->productService->get($id);
        return $this->successWithData($product);
    }

    public function destroy($id)
    {
        $deleted = $this->productService->destroy($id);
        return $this->succesWithMessage($deleted);
    }

    public function create(Request $request)
    {
        $created = $this->productService->create($request);
        return $this->successWithData($created);
    }

    public function patch(Request $request)
    {
        $patched = $this->productService->patch($request);
        return $this->successWithData($patched);
    }

    public function redeem(Request $request)
    {
        $user = Auth::user()->id;
        $this->productService->redeem($request->id, $request, $user);
        return $this->succesWithMessage('Berhasil Redeem!');
    }
}
