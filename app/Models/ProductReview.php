<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model
{
    protected $fillable = [
        'product',
        'user',
        'star',
        'review',
        'created_at',
        'updated_at'
    ];
}
