<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RedeemLog extends Model
{
    protected $table = 'redeem_logs';

    protected $fillable = [
        'user',
        'product',
        'quantity',
        'created_at',
        'updated_at'
    ];


    public function images()
    {
        return $this->hasMany(ProductImage::class, 'product');
    }
}
