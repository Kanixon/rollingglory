<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function (Request $request) {
});

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', 'Auth\AuthController@login');
    Route::post('/logout', 'Auth\AuthController@logout');
});

Route::group(['prefix' => 'user','middleware' => 'shouldLogin'], function () {
    Route::get('/', 'UserController@getAll');
    Route::post('/', 'UserController@create');
    Route::get('/{id}', 'UserController@get');
    Route::delete('/{id}', 'UserController@destroy');
});

Route::group(['prefix' => 'gifts','middleware' => 'shouldLogin'], function () {
    Route::get('/', 'ProductController@getAll');
    Route::post('/', 'ProductController@create');
    Route::get('/{id}', 'ProductController@get');
    Route::delete('/{id}', 'ProductController@destroy');
    Route::patch('/{id}', 'ProductController@patch');
    Route::post('/{id}/redeem', 'ProductController@redeem');
});

Route::group(['prefix' => 'dvlpr','middleware' => 'shouldLogin'], function () {
    Route::get('/', 'DvlprController@index');
});
