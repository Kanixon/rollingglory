<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Services\AuthService;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected AuthService $authService;

    public function __construct(
        AuthService $authService
    ) {
        $this->authService = $authService;
    }

    public function login(Request $request)
    {
        $token = $this->authService->handleLogin($request);
        return $this->successWithData($token);
    }

    public function logout(Request $request)
    {
        return $this->authService->handleLogout();
    }
}
