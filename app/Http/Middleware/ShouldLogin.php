<?php

namespace App\Http\Middleware;

use App\Exceptions\MyException;
use App\Models\AuthToken;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class ShouldLogin
{
    protected $auth;

    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $guard = null)
    {
        $token = $request->bearerToken();
        $user = User::join('auth_tokens', 'users.id', '=', 'auth_tokens.user')->first();

        if (!$token || !$user) {
            throw new MyException('Token Tidak Valid!');
        }

        Auth::setUser($user);
        return $next($request);
    }
}
