<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductLike extends Model
{
    protected $fillable = [
        'product',
        'user',
        'created_at',
        'updated_at'
    ];
}
