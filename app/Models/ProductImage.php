<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $fillable = [
        'product',
        'image',
        'created_at',
        'updated_at'
    ];
}
