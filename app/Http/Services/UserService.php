<?php

namespace App\Http\Services;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserService
{
    // TODO : Create validator for each input

    public function getAll(object $request)
    {
        $users = new User();

        if ($request->offset) {
            $users = $users->offset($request->offset);
        }
        
        if ($request->limit) {
            $users = $users->limit($request->limit);
        }

        $users = $users->get();
        return $users;
    }

    public function get(int $id)
    {
        return User::where(['id' => $id])->first();
    }

    public function destroy(int $id)
    {
        return User::where(['id' => $id])->delete();
    }

    public function create(object $input)
    {
        return User::create([
        'first_name' => $input->first_name,
        'last_name' => $input->last_name,
        'username' => $input->username ,
        'password' => Hash::make($input->password),
        ]);
    }
}
