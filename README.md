## Tech Stack

- Laravel
- Mysql

## Author

- **[Yunixon Tanone](https://www.linkedin.com/in/yunixon/)**

## How To Use

- Run composer install
- Import Database
- Change .env.example to .env
- change database username and password value in .env as in your local
- run php artisan serve
