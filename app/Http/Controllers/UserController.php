<?php

namespace App\Http\Controllers;

use App\Http\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected UserService $userService;

    public function __construct(
        UserService $userService
    ) {
        $this->userService = $userService;
    }

    public function getAll(Request $request)
    {
        $users = $this->userService->getAll($request);
        return $this->successWithData($users);
    }

    public function get($id)
    {
        $user = $this->userService->get($id);
        return $this->successWithData($user);
    }

    public function destroy($id)
    {
        $deleted = $this->userService->destroy($id);
        return $this->succesWithMessage($deleted);
    }

    public function create(Request $request)
    {
        $created = $this->userService->create($request);
        return $this->successWithData($created);
    }
}
