<?php

namespace App\Http\Services;

use App\Exceptions\MyException;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\RedeemLog;

class ProductService
{
    // TODO : Create validator for each input

    public function getAll(object $request)
    {
        $products = new Product();

        if ($request->offset) {
            $products = $products->offset($request->offset);
        }
        
        if ($request->limit) {
            $products = $products->limit($request->limit);
        }

        $products = $products->with('images');

        $products = $products->get();
        return $products;
    }

    public function get(int $id)
    {
        return Product::where(['id' => $id])->with('images')->first();
    }

    public function destroy(int $id)
    {
        return Product::where(['id' => $id])->delete();
    }

    public function create(object $input)
    {
        $created = Product::create([
            'name' => $input->name,
            'stock' => $input->stock,
            'description' => $input->description,
            'category' => $input->category,
        ]);

        if ($input->images) {
            foreach ($input->images as $image) {
                ProductImage::create([
                    'product' => $created->id,
                    'image' => $image
                ]);
            }
        }

        return $created;
    }

    public function patch($input)
    {
    }

    public function redeem($product_id, $input, $user)
    {
        $product = $this->get($product_id);

        $quantity = $input->quantity ?? 0;

        if (!$product) {
            throw new MyException('Product Tidak Ditemukan!');
        }

        if (!$quantity) {
            throw new MyException('Quantity Tidak Valid!');
        }

        if ($product->stock < $quantity) {
            throw new MyException('Stock Tidak Cukup!');
        }

        // If Success, Add Log Redeem

        RedeemLog::create([
        'user' => $user,
        'product' => $product_id,
        'quantity' => $quantity,
        ]);

        // Selesai Redeem, Kurangi Stock Product
        Product::where(['id' => $product_id])->update([
            'stock' => ($product->stock - $quantity)
        ]);
    }
}
