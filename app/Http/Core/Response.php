<?php

namespace App\Http\Core;

use App\Exceptions\MyException;

// TODO : This Should Be Improved!
trait Response
{
    protected $successCode = 1;
    protected $errorCode = 0;

    public function succesWithMessage(string $message = "")
    {
        $response = [
            'status' => $this->successCode,
            'message' => $message,
            'data' => null
        ];

        return $this->toJson($response);
    }

    public function errorWithMessage(string $message = ""): string
    {
        $response = [
            'status' => $this->errorCode,
            'message' => $message,
            'data' => null
        ];

        return $this->toJson($response);
    }

    public function errorWithData($data = null): string
    {
        $response = [
            'status' => $this->errorCode,
            'message' => '',
            'data' => $data
        ];

        return $this->toJson($response);
    }

    public function successWithData($data = null): string
    {
        $response = [
            'status' => $this->successCode,
            'message' => 'Berhasil Ambil Data!',
            'data' => $data
        ];

        return $this->toJson($response);
    }

    private function toJson(array $data = [])
    {
        return json_encode($data);
    }
}
