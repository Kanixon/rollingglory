-- MySQL dump 10.13  Distrib 5.7.36, for Linux (x86_64)
--
-- Host: localhost    Database: marketplace
-- ------------------------------------------------------
-- Server version	5.7.36

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_tokens`
--

CREATE DATABASE `marketplace` /*!40100 DEFAULT CHARACTER SET latin1 */;

DROP TABLE IF EXISTS `auth_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_tokens` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user` bigint(20) NOT NULL,
  `token` varchar(255) NOT NULL,
  `expire` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `auth_tokens_FK` (`user`),
  CONSTRAINT `auth_tokens_FK` FOREIGN KEY (`user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_tokens`
--

LOCK TABLES `auth_tokens` WRITE;
/*!40000 ALTER TABLE `auth_tokens` DISABLE KEYS */;
INSERT INTO `auth_tokens` VALUES (8,7,'$2y$10$QjNe59iJX8sMeXYpjtKpB.iJ2HEFDX2NaM51TuQ4aAuQiwuW3GQJW','2022-01-21 17:59:10','2022-01-20 17:59:10','2022-01-20 17:59:10'),(9,7,'$2y$10$Rrsysum6c6UNBfMN02MiR.xD6QE/XwsAwKX9UEomfmw6FmRdfGT2C','2022-01-21 18:00:52','2022-01-20 18:00:52','2022-01-20 18:00:52'),(10,7,'$2y$10$OO/zRXcBhUW5HME0HtApYONp/R/MjsYC5w4V4lkMMIojCc0VBq.DW','2022-01-21 18:00:54','2022-01-20 18:00:54','2022-01-20 18:00:54'),(11,7,'$2y$10$ZlpsB1X1EVWmPKZcGCfrt.TgsUShlSdy3bQfxyYbcKN2bq4WpJ1/K','2022-01-21 18:00:58','2022-01-20 18:00:58','2022-01-20 18:00:58'),(12,7,'$2y$10$0GbbpVIhZMf4N5EKqOhWieNY8a3h5HuipU.pVKOEx38ca2NyzbG.O','2022-01-21 18:00:59','2022-01-20 18:00:59','2022-01-20 18:00:59'),(13,7,'$2y$10$O11.VBKRQ2ox.bxyuqFdxevRMgJ7D8S52gVbbBajk0WU4fwUVrVHe','2022-01-21 18:01:00','2022-01-20 18:01:00','2022-01-20 18:01:00'),(14,7,'$2y$10$QpezFasJVfDeLg2drCTlfuCRFJNs5A8cNPIW4RvObnJYHDhRLun3W','2022-01-21 18:01:01','2022-01-20 18:01:01','2022-01-20 18:01:01'),(15,7,'$2y$10$WhTapKaiBvGOU0QHrlyayeJGnC6XoFVSqOqVc80A.vnbI6nfYWxhO','2022-01-21 18:01:02','2022-01-20 18:01:02','2022-01-20 18:01:02'),(16,7,'$2y$10$916uhSVAQG/rTNASLZ8yWeN.Yi2ppTKee2gfh27gJy/qqpNNO.p8u','2022-01-22 13:47:15','2022-01-21 13:47:15','2022-01-21 13:47:15');
/*!40000 ALTER TABLE `auth_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persons`
--

DROP TABLE IF EXISTS `persons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persons` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persons`
--

LOCK TABLES `persons` WRITE;
/*!40000 ALTER TABLE `persons` DISABLE KEYS */;
/*!40000 ALTER TABLE `persons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_images`
--

DROP TABLE IF EXISTS `product_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_images` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product` bigint(20) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_images_FK` (`product`),
  CONSTRAINT `product_images_FK` FOREIGN KEY (`product`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_images`
--

LOCK TABLES `product_images` WRITE;
/*!40000 ALTER TABLE `product_images` DISABLE KEYS */;
INSERT INTO `product_images` VALUES (18,18,'images1.png','2022-01-21 12:17:01','2022-01-21 12:17:01'),(28,27,'images1.png','2022-01-21 12:22:41','2022-01-21 12:22:41'),(29,27,'images2.png','2022-01-21 12:22:41','2022-01-21 12:22:41'),(30,28,'images1.png','2022-01-21 12:22:43','2022-01-21 12:22:43'),(31,28,'images2.png','2022-01-21 12:22:43','2022-01-21 12:22:43'),(34,30,'images1.png','2022-01-21 12:23:00','2022-01-21 12:23:00'),(35,30,'images2.png','2022-01-21 12:23:00','2022-01-21 12:23:00'),(36,31,'images1.png','2022-01-21 13:47:59','2022-01-21 13:47:59'),(37,31,'images2.png','2022-01-21 13:47:59','2022-01-21 13:47:59'),(38,32,'images1.png','2022-01-21 13:51:21','2022-01-21 13:51:21'),(39,32,'images2.png','2022-01-21 13:51:21','2022-01-21 13:51:21');
/*!40000 ALTER TABLE `product_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `description` text,
  `category` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (14,'Samsung Note 9',1,'Ini description','phone','2022-01-21 12:14:17','2022-01-21 12:14:17'),(15,'Samsung Note 9',1,'Ini description','phone','2022-01-21 12:15:03','2022-01-21 12:15:03'),(16,'Samsung Note 9',1,'Ini description','phone','2022-01-21 12:16:34','2022-01-21 12:16:34'),(17,'Samsung Note 9',1,'Ini description','phone','2022-01-21 12:16:48','2022-01-21 12:16:48'),(18,'Samsung Note 9',1,'Ini description','phone','2022-01-21 12:17:01','2022-01-21 12:17:01'),(19,'Samsung Note 9',1,'Ini description','phone','2022-01-21 12:18:40','2022-01-21 12:18:40'),(20,'Samsung Note 9',1,'Ini description','phone','2022-01-21 12:18:55','2022-01-21 12:18:55'),(21,'Samsung Note 9',1,'Ini description','phone','2022-01-21 12:19:12','2022-01-21 12:19:12'),(22,'Samsung Note 9',1,'Ini description','phone','2022-01-21 12:19:23','2022-01-21 12:19:23'),(23,'Samsung Note 9',1,'Ini description','phone','2022-01-21 12:19:32','2022-01-21 12:19:32'),(24,'Samsung Note 9',1,'Ini description','phone','2022-01-21 12:20:33','2022-01-21 12:20:33'),(25,'Samsung Note 9',1,'Ini description','phone','2022-01-21 12:20:34','2022-01-21 12:20:34'),(26,'Samsung Note 9',1,'Ini description','phone','2022-01-21 12:21:13','2022-01-21 12:21:13'),(27,'Samsung Note 9',1,'Ini description','phone','2022-01-21 12:22:41','2022-01-21 12:22:41'),(28,'Samsung Note 9',5,'Ini description','phone','2022-01-21 12:22:43','2022-01-21 13:51:15'),(30,'Samsung Note 9',1,'Ini description','phone','2022-01-21 12:23:00','2022-01-21 12:23:00'),(31,'Samsung Note 9',1,'Ini description','phone','2022-01-21 13:47:59','2022-01-21 13:47:59'),(32,'Samsung Note 9',1,'Ini description','phone','2022-01-21 13:51:21','2022-01-21 13:51:21');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `redeem_logs`
--

DROP TABLE IF EXISTS `redeem_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `redeem_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user` bigint(20) DEFAULT NULL,
  `product` bigint(20) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `redeem_logs_FK` (`user`),
  KEY `redeem_logs_FK_1` (`product`),
  CONSTRAINT `redeem_logs_FK` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `redeem_logs_FK_1` FOREIGN KEY (`product`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `redeem_logs`
--

LOCK TABLES `redeem_logs` WRITE;
/*!40000 ALTER TABLE `redeem_logs` DISABLE KEYS */;
INSERT INTO `redeem_logs` VALUES (6,8,28,1,'2022-01-21 13:08:39','2022-01-21 13:08:39'),(7,8,28,1,'2022-01-21 13:08:42','2022-01-21 13:08:42'),(8,8,28,1,'2022-01-21 13:51:15','2022-01-21 13:51:15');
/*!40000 ALTER TABLE `redeem_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (7,'Yunixon','Tanone','Yunixon','$2y$10$JX.K0v2chf/o.7Gsxuijy.IXlVn6tAVbfOw7xd4kziHVRdZaorwNG','2022-01-20 17:59:00','2022-01-20 17:59:00'),(8,NULL,NULL,NULL,'$2y$10$c0pGgqlvq/zAlk3O.O6Uyu8db1d/r7zfrherRuAlOQY9naWYNxRiK','2022-01-21 10:49:58','2022-01-21 10:49:58');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-22  4:01:35
