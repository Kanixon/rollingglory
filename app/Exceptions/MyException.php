<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class MyException extends Exception
{
    protected $message;

    public function __construct($message = null)
    {
        $this->message = $message;
    }
    
    public function render()
    {
        return response()->json([
            "message" => $message ?? $this->getMessage(),
            "status" => 'Error Exception!'
        ]);
    }
}
