<?php

namespace App\Http\Services;

use App\Exceptions\MyException;
use App\Models\AuthToken;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class AuthService
{
    protected const EXPIRE_TIME = 24; // 24 Hour

    public function handleLogin($request)
    {
        $user = User::where(['username' => $request->username])->first();

        if (!$user) {
            throw new MyException('User Not Found!');
        }

        $is_password_valid = Hash::check($request->password, $user->password);

        if (!$is_password_valid) {
            throw new MyException('Password Is Wrong!');
        }

        $new_token = Hash::make($user->id . $user->name . now());
        AuthToken::create([
            'user' => $user->id,
            'token' => $new_token,
            'expire' => Carbon::now()->add(self::EXPIRE_TIME, 'hour')
        ]);

        return [
            'token' => $new_token
        ];
    }

    public function handleLogout()
    {
        return;
    }
}
