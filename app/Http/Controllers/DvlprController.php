<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DvlprController extends Controller
{
    public function index(Request $request)
    {
        return $this->successWithData('$users');
    }
}
